ARG NATS_VER=2.9-alpine

FROM nats:${NATS_VER}

RUN apk add --no-cache curl wget unzip

# instalamos la ultima version de nats desde github
ARG NATSCLI_VER=0.0.35
RUN wget https://github.com/nats-io/natscli/releases/download/v${NATSCLI_VER}/nats-${NATSCLI_VER}-linux-amd64.zip && \
    unzip nats-${NATSCLI_VER}-linux-amd64.zip && \
    mv nats-${NATSCLI_VER}-linux-amd64/nats /usr/local/bin && \
    rm nats-${NATSCLI_VER}-linux-amd64.zip && \
    rm -rf nats-${NATSCLI_VER}-linux-amd64
